

import 'package:flutter/material.dart';



class AppColor {
  static const blue = Color(0xff2337f0);
  static const navy = Color(0xff100f7c);
  static const cayan = Color(0xff0ed6d6);
  static const bg = Color(0x0d2337f0);
  static const yellow = Color(0xfff7d762);
  static const white = Color(0xffffffff);
  static const purple = Color(0xff7c20e5);
  static const grey = Color(0xffa1a1a1);
  static const blue10 = Color(0x402337f0);
  static const black = Color(0xff282828);
  static const secondaryFillColorLight = Color(0x29787880);
  static const green = Color(0xff78e587);
  static const red = Color(0xffe56777);
  static const navigationBarIconsRegular = Color(0xffc4c4c4);
  static const fillBlueBlue50 = Color(0x802337f0);
  static const fillBlueBlue75 = Color(0xbf2337f0);
  static const fillBlueBlue10 = Color(0x1a2337f0);
  static const fillCayanCayan75 = Color(0xbf0ed6d6);
  static const fillCayanCayan25 = Color(0x400ed6d6);
  static const fillCayanCayan50 = Color(0x800ed6d6);
  static const fillCayanCayan10 = Color(0x1a0ed6d6);
  static const fillRedRed75 = Color(0xbfe56777);
  static const fillRedRed50 = Color(0x80e56777);
  static const fillRedRed25 = Color(0x40e56777);
  static const fillYellowYellow75 = Color(0xbff7d762);
  static const fillRedRed10 = Color(0x1ae56777);
  static const fillYellowYellow50 = Color(0x80f7d762);
  static const fillYellowYellow25 = Color(0x40f7d762);
  static const fillYellowYellow10 = Color(0x1af7d762);
  static const fillPurplePurple75 = Color(0xbe7c20e5);
  static const fillPurplePurple25 = Color(0x3f7c20e5);
  static const fillPurplePurple50 = Color(0x7f7c20e5);
  static const fillPurplePurple10 = Color(0x1b7c20e5);
  static const fillGreenGreen75 = Color(0xc078e587);
  static const fillGreenGreen50 = Color(0x7f78e587);
  static const fillGreenGreen25 = Color(0x4078e587);
  static const fillGreenGreen10 = Color(0x1978e587);
  static const fillBlackBlack75 = Color(0xbf282828);
  static const fillBlackBlack50 = Color(0x80282828);
  static const fillBlackBlack25 = Color(0x40282828);
  static const fillBlackBlack10 = Color(0x1a282828);
  static const fillNavyNavy50 = Color(0x80100f7c);
  static const fillWhiteWhite50 = Color(0x80ffffff);
  static const fillNavyNavy10 = Color(0x1a100f7c);
  static const fillClear = Color(0x000080ff);
  static const fillNavyNavy5 = Color(0x0e100f7c);
  static const fillNavyNavy25 = Color(0x3e100f7c);
}