

import 'package:flutter/material.dart';
import '../utils/size_config.dart';
import 'app_color.dart';

/*
FontSize Estimates:
32 -> fontHuge * 0.8
20 -> fontLarge * 0.9
18 -> fontMedium
16 -> fontMedium * 0.9
14 -> fontSmallPlus
12 -> fontSmall
*/

class TextStyles {
  static final chosenContactTypeTextStyle = TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontSmallPlus);

  static final notChosenContactTypeTextStyle = TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontSmallPlus);

  static final titlesH516NavyRight = TextStyle(
      fontFamily: 'SimplerPro',
      color: const Color(0xff100f7c),
      fontSize: SizeConfig.fontMedium * 0.9,
      fontWeight: FontWeight.w400,
      fontStyle: FontStyle.normal);

  static final statusTextStyleCenter = TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontSmallPlus);

  static const appBarTitleTextStyle = TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: 18.0);

  static final dealTitleTextStyleCenter = TextStyle(
    fontFamily: 'SimplerPro',
    color: const Color(0xff282828),
    fontSize: SizeConfig.fontSmallPlus,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
  );

  static final assetTitleTextStyleCenter = TextStyle(
    fontFamily: 'SimplerPro',
    color: const Color(0xff282828),
    fontSize: SizeConfig.fontSmallPlus,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
  );

  static final addressTextStyleCenter = TextStyle(
      color: AppColor.navy,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final registerTextStyleCenter = TextStyle(
      color: AppColor.navy,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium);

  static final checkBoxTextStyleCenter = TextStyle(
    color: const Color(0xcc100f7c),
    fontWeight: FontWeight.w400,
    fontFamily: "SimplerPro",
    fontStyle: FontStyle.normal,
    fontSize: SizeConfig.fontMedium * 0.9,
  );

  static final emptyStateTextStyleCenter = TextStyle(
    color: const Color(0xcc100f7c),
    fontWeight: FontWeight.w400,
    fontFamily: "SimplerPro",
    fontStyle: FontStyle.normal,
    fontSize: SizeConfig.fontMedium * 0.9,
  );

  static final titlesH132WhiteCenter = TextStyle(
      color: AppColor.white,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontHuge * 0.8);

  static final titlesH132NavyCenter = TextStyle(
      color: const Color(0xff18036b),
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontHuge * 0.8);

  static final titlesH220NavyCenter = TextStyle(
      color: const Color(0xff18036b),
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontLarge * 0.9);

  static final titlesH220BlackCenter = TextStyle(
      color: AppColor.black,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontLarge * 0.9);

  static final titlesH220BlackRight = TextStyle(
      color: AppColor.black,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontLarge * 0.9);

  static final titlesH220WhiteRight = TextStyle(
      color: AppColor.white,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontLarge * 0.9);

  static final titlesH220WhiteCenter = TextStyle(
      color: AppColor.white,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontLarge * 0.9);

  static final cTABlue = TextStyle(
      color: AppColor.blue,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium);

  static final titlesH318ColorsBlue = TextStyle(
      color: AppColor.blue,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium);

  static final formsQuestionNo = TextStyle(
      color: AppColor.navy,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium);

  static final titlesH318NavyCenter = TextStyle(
      color: AppColor.navy,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium);

  static final titlesH318WhiteCenter = TextStyle(
      color: AppColor.white,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium);

  static final formsQuestionYes = TextStyle(
      color: AppColor.white,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium);

  static final titlesH318WhiteRight = TextStyle(
      color: AppColor.white,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium);

  static final titlesH318ColorsPurple = TextStyle(
      color: AppColor.purple,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium);

  static final headerRight = TextStyle(
      color: AppColor.white,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium);

  static final headerCenter = TextStyle(
      color: AppColor.white,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium);

  static final titlesH318BlackRight = TextStyle(
      color: const Color(0xff3b3b3b),
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium);

  static final titlesH318BlueLeft = TextStyle(
      color: AppColor.blue,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium);

  static final titlesH318NavyRight = TextStyle(
      color: AppColor.navy,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium);

  static final titlesH318BlueCenter = TextStyle(
      color: AppColor.blue,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium);

  static final titlesH318BlackCenter = TextStyle(
      color: AppColor.black,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium);

  static final titlesH31818px = TextStyle(
      color: AppColor.black,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium);

  static final titlesH416RegularBlackCenter = TextStyle(
      color: AppColor.black,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final titlesH416WhiteRight = TextStyle(
      color: AppColor.white,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final cTAWhite = TextStyle(
      color: AppColor.white,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final titlesH416BlackRight = TextStyle(
      color: AppColor.black,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final cTANavy = TextStyle(
      color: const Color(0xff18036b),
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final titlesH416RegularBlueCenter = TextStyle(
      color: AppColor.blue,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final titlesH416NavyRight = TextStyle(
      color: AppColor.navy,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final titlesH416BlackCenter = TextStyle(
      color: AppColor.black,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final titlesH416WhiteLeft = TextStyle(
      color: AppColor.white,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final titlesH416NavyCenter = TextStyle(
      color: AppColor.navy,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final titlesH416WhiteCenter = TextStyle(
      color: AppColor.white,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final titlesH416WhiteCenterDisabled = TextStyle(
      color: AppColor.grey,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final titlesH416NavyLeft = TextStyle(
      color: AppColor.navy,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final titlesH516NavyCenter = TextStyle(
      color: AppColor.navy,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final paragraphErrorCenter = TextStyle(
      color: AppColor.red,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final paragraphNavyLeft = TextStyle(
      color: const Color(0xcc100f7c),
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final infoText14pxRight = TextStyle(
      color: const Color(0xcc100f7c),
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontSmallPlus);

  static final paragraphWhiteCenter = TextStyle(
      color: AppColor.white,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final formsTextField = TextStyle(
      color: AppColor.red,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final titlesH516WhiteCenter = TextStyle(
      color: AppColor.white,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final cTALink = TextStyle(
      color: AppColor.navy,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final formsDataText = TextStyle(
      color: AppColor.navy,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final formsTextFieldHint = TextStyle(
      color: const Color(0x99100f7c),
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final formsTextFieldText = TextStyle(
      color: const Color(0xff18036b),
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final titlesH516RegularRight = TextStyle(
      color: AppColor.black,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final paragraphRegularRight = TextStyle(
      color: AppColor.black,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final paragraphRegularCenter = TextStyle(
      color: AppColor.black,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final paragraphRegularLeft = TextStyle(
      color: AppColor.black,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final paragraphNavyCenter = TextStyle(
      color: const Color(0xcc100f7c),
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final paragraphNavyCenterMediumPlus  = TextStyle(
      color: const Color(0xcc100f7c),
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMediumPlus * 0.9);


  static final titlesH516RegularCenter = TextStyle(
      color: const Color(0xff222222),
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final paragraphWhiteRight = TextStyle(
      color: AppColor.white,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final paragraphNavyRight = TextStyle(
      color: const Color(0xcc100f7c),
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontMedium * 0.9);

  static final toggleTabsOn = TextStyle(
      color: AppColor.white,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontSmallPlus);

  static final tabsSelected = TextStyle(
      color: AppColor.white,
      fontWeight: FontWeight.w700,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontSmallPlus);

  static final formsTextFieldLabel2 = TextStyle(
      color: const Color(0xe6282828),
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontSmallPlus);

  static final toggleTabsOff = TextStyle(
      color: AppColor.navy,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontSmallPlus);

  static final formsTextFieldLabel = TextStyle(
      color: const Color(0xe6282828),
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontSmallPlus);

  static final formsTextFieldComment = TextStyle(
      color: const Color(0xff3b3b3b),
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontSmallPlus);

  static final formsDataLabel = TextStyle(
      color: AppColor.black,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontSmallPlus);

  static final tabsUnselected = TextStyle(
      color: const Color(0xbfffffff),
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontSmallPlus);

  static final formsTextFieldError = TextStyle(
      color: const Color(0xffff0000),
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontSmallPlus);

  static final paragraphGreyCenter = TextStyle(
      color: const Color(0xff989898),
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontSmallPlus);

  static final paragraphGreyLeft = TextStyle(
      color: const Color(0xff989898),
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontSmallPlus);

  static final paragraphGreyRight = TextStyle(
      color: const Color(0xff989898),
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontSmallPlus);

  static final navigationBarChosen = TextStyle(
      color: AppColor.navy,
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontSmall);

  static final navigationBarDefault = TextStyle(
      color: const Color(0x8018036b),
      fontWeight: FontWeight.w400,
      fontFamily: "SimplerPro",
      fontStyle: FontStyle.normal,
      fontSize: SizeConfig.fontSmall);

  static final lightNavy = TextStyle(
    color: const Color(0xcc100f7c),
    fontWeight: FontWeight.w400,
    fontFamily: "SimplerPro",
    fontStyle: FontStyle.normal,
    fontSize: SizeConfig.fontSmall,
  );
}
