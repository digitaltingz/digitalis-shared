class Regex {
  static String phoneRegex = r'^\+?(972|0)(\-)?0?(([23489]{1}\d{7})|[5]{1}\d{8})$';
  static String emailRegex = r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
  static String letterRegex = r'[\u0590-\u05fe]';
}