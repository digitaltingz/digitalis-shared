

import 'package:intl/intl.dart';



class Formatters {
  static String dateToFormattedString(DateTime? date) {
    DateFormat formatter = DateFormat("dd-MM-yyyy");
    return formatter.format(date ?? DateTime(1990, 0, 0));
  }

  static String dateToFormattedStringUS(DateTime? date) {
    DateFormat formatter = DateFormat("MM-dd-yyyy");
    return formatter.format(date ?? DateTime(1990, 0, 0));
  }
}
