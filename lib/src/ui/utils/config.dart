

class Config {
  // appbar heights
  static const  double appBarDefaultHeight = 70.0;

  static const  String baseUrl = 'https://digitalis-server.azurewebsites.net';
  static const  String wpBaseUrl = 'https://digitalisdi.com/wp-json/app';

  static const String tokenKey = 'token';
  static const String isLoggedInKey = 'loggedIn';
  static const String lastNameKey = 'lastName';
  static const String firstNameKey = 'firstName';
  static const String profilePictureKey = 'profilePicture';
  static const String agencyPictureKey = 'agencyPicture';
  static const String dictionaryKey = "Dictionary";


  // end-points
  static const  String sendSMSEndPoint = '/api/agent/sendSMS';
  static const  String loginEndPoint = '/api/auth/login';
  static const  String sendOtpEndPoint = '/api/agent/sendOtpCode';
  static const  String verifyOtpEndPoint = '/api/agent/codeOtpValidation';

  static const  String dictionaryEndPointAgentApp = '/GetTable?table_id=1';
  static const  String dictionaryEndPointBuyerSellerApp = '/GetTable?table_id=2';

  static const  String createAgentEndPoint = '/api/agent/createAgent';
  static const  String getAgentDetailsEndPoint = '/api/agent/getAgentDetails/';
  static const  String updateAgentDetailsEndPoint = '/api/agent/updateAgentDetails/';
  static const  String getAgencyListEndPoint = '/api/agent/getAgencyList/';
  static const  String getCitiesListEndPoint = '/api/agent/getCitiesList/';
  static const  String addAssetBuyerEndPoint = '/api/agent/addAssetBuyer';

  static const  String getAssetsListEndPoint = '/api/agent/getAssetsList';

  static const  String getAssetTypeEndPoint = '/api/agent/getAssetTypesList';

  static const  String old_getDealsListEndPoint = '/api/agent/getDealsList?contactType=100000004';

  static const  String getDealsListEndPoint = '/api/agent/v2/getDealsList';

  static const  String addPropertyOwnerEndPoint = '/api/agent/addPropertyOwnerToDeal';

  static const  String addCorporateEndPoint = '/api/agent/addCorporateToDeal';

  static const  String addAssetEndPoint = '/api/agent/addAssetNDeal';

  // request retries8494
  static const  int maxRequestAttempts = 3;
  static const  int delayFactor = 2000;
  static const  int durationMilliseconds = 40000;


  // menu items links
  static const  String fees_link = 'https://digitalisdi.com/fees/';
  static const  String contact_us_link = 'https://digitalisdi.com/contact-us/';
  static const  String terms_link = 'https://digitalisdi.com/terms/';
  static const  String home_link = 'https://digitalisdi.com/home/';
}
