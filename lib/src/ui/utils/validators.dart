import 'regex.dart';

class Validators {
  static String? emptyValidator(String? value) {
    if (value!.isEmpty) {
      return 'שדה חובה';
    }
    return null;
  }

  static String? emailValidator(String? value) {
    bool emailValid = RegExp(Regex.emailRegex).hasMatch(value!);
    if (!emailValid) {
      return 'כתובת מייל לא תקינה';
    }
    return null;
  }

  static String? phoneValidator(String? value) {
    // if (!value!.contains(RegExp(Regex.phoneRegex))) We won't need this as we control the phone prefix through Dropdown.
    //   return 'מספר פלאפון לא תקין';
    if (value!.length != 7) {
      return 'מספר פלאפון לא תקין';
    }
    return null;
  }

  static String? numberValidator(String? value) {
    final number = int.tryParse(value!);
    if (number == null) return 'השדה חייב להכיל מספר בלבד';
    return null;
  }

  static String? notRequiredValidator(String? value) {
    return null;
  }
}
