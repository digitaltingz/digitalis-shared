
extension BeautifyPrice on String {
  String beautifyPrice() {
    return '₪' + this.replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]},');
  }
}