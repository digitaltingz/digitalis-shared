enum IndicatorState {
  ACTIVE,
  INACTIVE,
  VISIT,
}

enum ContactType {
  OWNER,
  CONTACT,
  CORPORATION,
}

enum NotificationType {
  SUCCESS,
  ERROR,
  INFORMATION,
}