import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../../digitalis_shared.dart';


class BottomDialog {
  static Future<void> createPopup({
    required BuildContext context,
    required String title,
    required String message,
    String buttonText = 'הבנתי, תודה',
    required NotificationType notificationType,
    Function? onTap,
  }) {
    return showModalBottomSheet(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(24.0),
          topLeft: Radius.circular(24.0),
        ),
      ),
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return Container(
          height: SizeConfig.screenHeight * 0.8,
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              notificationType == NotificationType.ERROR
                  ? SvgPicture.asset('asset/images/error_illustration.svg')
                  : notificationType == NotificationType.SUCCESS
                  ? SvgPicture.asset(
                  'asset/images/success_illustration.svg')
                  : SvgPicture.asset('asset/images/info_illustration.svg'),
              Text(
                title,
                style: TextStyles.titlesH220NavyCenter,
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 10),
              Text(
                message,
                style: TextStyles.paragraphNavyCenter,
                textAlign: TextAlign.center,
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: CustomButton(
                    largerWidth: true,
                    onTap: () {
                      Navigator.pop(context);
                      onTap?.call();
                    },
                    text: buttonText,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  static Future<void> createCloseAppDialog({
    required BuildContext context,
    required String title,
    String closeText = 'כן',
    String cancelText = 'לא',
    Function? onCancel,
    Function? onClose,
  }) {
    return showModalBottomSheet(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(24.0),
          topLeft: Radius.circular(24.0),
        ),
      ),
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return Container(
          height: SizeConfig.screenHeight * 0.8,
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset('asset/images/info_illustration.svg'),
              Text(
                title,
                style: TextStyles.titlesH220NavyCenter,
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 10),
              Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      CustomButton(
                        isFilled: false,
                        onTap: () {
                          Navigator.pop(context);
                          onClose?.call();
                        },
                        text: closeText,
                      ),
                      CustomButton(
                        onTap: () {
                          Navigator.pop(context);
                          onCancel?.call();
                        },
                        text: cancelText,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  static Future<void> createPopupWithCheckbox(
      {required BuildContext context,
        required String title,
        required String message,
        required String firstButtonText,
        required String secondButtonText,
        required NotificationType notificationType,
        required Function firstButtonOnTap,
        required Function secondButtonOnTap,
        bool ischeckboxRequired = false}) {
    bool isChecked = false;

    return showModalBottomSheet(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(24.0),
          topLeft: Radius.circular(24.0),
        ),
      ),
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
            return Container(
              height: SizeConfig.screenHeight * 0.8,
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset('asset/images/info_illustration.svg'),
                  Text(
                    title,
                    style: TextStyles.titlesH220NavyCenter,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ClipRRect(
                        clipBehavior: Clip.hardEdge,
                        borderRadius: BorderRadius.all(Radius.circular(6)),
                        child: SizedBox(
                          width: 27,
                          height: 27,
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: AppColor.navy,
                                width: 2,
                              ),
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Theme(
                              data: ThemeData(
                                unselectedWidgetColor: Colors.transparent,
                              ),
                              child: Checkbox(
                                  fillColor: MaterialStateProperty.all<Color>(
                                      Colors.transparent),
                                  checkColor: AppColor.navy,
                                  value: isChecked,
                                  onChanged: (bool? value) {
                                    setState(() {
                                      isChecked = value!;
                                    });
                                  }),
                            ),
                          ),
                        ),
                      ),

                      const SizedBox(
                        width: 10,
                      ),
                      Flexible(
                        child: Text(
                          message,
                          style: TextStyles.paragraphNavyCenter,
                          textAlign: TextAlign.start,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 5),
                  Expanded(
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CustomButton(
                            isFilled: false,
                            onTap: () {
                              firstButtonOnTap();
                            },
                            text: firstButtonText,
                          ),
                          CustomButton(
                            isEnabled: isChecked,
                            onTap: () {
                              secondButtonOnTap();
                            },
                            text: secondButtonText,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}

