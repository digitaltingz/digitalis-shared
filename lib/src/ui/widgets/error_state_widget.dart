

import 'package:digitalis_shared/src/ui/styles/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ErrorStateWidget extends StatelessWidget {
  final String title;
  final String content;

  const ErrorStateWidget({
    Key? key,
    this.title = 'אופס!',
    this.content = 'המערכת נתקלה בבעיה...'}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Flexible(
          child: SvgPicture.asset('asset/images/error_illustration.svg'),
        ),
        const SizedBox(height: 30),
        Flexible(
          child: Text(title,
              style: TextStyles.titlesH220NavyCenter,
              textAlign: TextAlign.center),
        ),
        const SizedBox(height: 10),
        Flexible(
          child: Text(
            content,
            style: TextStyles.emptyStateTextStyleCenter,
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }
}
