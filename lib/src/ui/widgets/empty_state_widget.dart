

import 'package:digitalis_shared/src/ui/styles/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class EmptyStateWidget extends StatelessWidget {
  final String imagePath;
  final String title;
  final String content;

  const EmptyStateWidget({
    Key? key,
    required this.imagePath,
    required this.title,
    required this.content}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Flexible(child: SvgPicture.asset(imagePath)),
        const SizedBox(height: 30),
        Flexible(
          child: Text(title,
              style: TextStyles.titlesH220NavyCenter,
              textAlign: TextAlign.center),
        ),
        const SizedBox(height: 10),
        Flexible(
          child: Text(
            content,
            style: TextStyles.emptyStateTextStyleCenter,
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }
}
