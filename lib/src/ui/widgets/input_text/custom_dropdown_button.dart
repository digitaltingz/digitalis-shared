

import 'package:digitalis_shared/src/ui/styles/app_color.dart';
import 'package:digitalis_shared/src/ui/styles/text_styles.dart';
import 'package:flutter/material.dart';


class CustomDropdownButton extends StatefulWidget {
  final double width;
  final String title;
  final bool editable;
  final List<String> items;
  final String currentValue;
  final void Function(String?) onChange;

  const CustomDropdownButton({
    Key? key,
    this.width = 180,
    this.title = '',
    this.editable = true,
    required this.items,
    required this.currentValue,
    required this.onChange}) : super(key: key);

  @override
  _CustomDropdownButtonState createState() => _CustomDropdownButtonState();
}

class _CustomDropdownButtonState extends State<CustomDropdownButton> {
  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: !widget.editable,
      child: SizedBox(
        height: 72,
        width: widget.width,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.title,
              style: TextStyles.formsTextFieldLabel2,
              textAlign: TextAlign.right,
            ),
            const SizedBox(height: 5),
            SizedBox(
              width: widget.width,
              child: Center(
                child: Container(
                  height: 35,
                  padding: const EdgeInsets.all(0.0),
                  child: Container(
                    width: widget.width,
                    padding: const EdgeInsets.symmetric(horizontal: 12.0),
                    decoration: BoxDecoration(
                        borderRadius: const BorderRadius.all(
                          Radius.circular(6.0),
                        ),
                        color: Theme.of(context).scaffoldBackgroundColor,
                        border: Border.all(color: AppColor.fillNavyNavy25)),
                    child: DropdownButton<String>(
                      icon: const Icon(Icons.keyboard_arrow_down,
                          color: AppColor.navy),
                      isExpanded: true,
                      underline: const SizedBox(),
                      alignment: Alignment.topCenter,
                      value: widget.currentValue.isNotEmpty
                          ? widget.currentValue
                          : widget.items[0],
                      style: TextStyles.formsTextFieldText,
                      items: widget.items
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: widget.onChange,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
