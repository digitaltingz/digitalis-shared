

import 'package:digitalis_shared/src/ui/styles/app_color.dart';
import 'package:digitalis_shared/src/ui/styles/text_styles.dart';
import 'package:digitalis_shared/src/ui/utils/formatters.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';


class CustomDateTextField extends StatefulWidget {
  final String title;
  final TextEditingController controller;
  final double width;
  final String icon;
  final ValueChanged<String>? onChanged;

  const CustomDateTextField({
    Key? key,
    required this.title,
    required this.controller,
    this.width = 180,
    this.icon = '',
    this.onChanged}) : super(key: key);

  @override
  State<CustomDateTextField> createState() => _CustomDateTextFieldState();
}

class _CustomDateTextFieldState extends State<CustomDateTextField> {
  late FocusNode focusNode;

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
    focusNode.addListener(
      () {
        setState(() {});
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 72,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.title,
              style: TextStyles.formsTextFieldLabel2,
              textAlign: TextAlign.right),
          const SizedBox(height: 5),
          Container(
            height: 35,
            width: widget.width,
            decoration: focusNode.hasFocus
                ? BoxDecoration(
                    borderRadius: const BorderRadius.all(
                      Radius.circular(6.0),
                    ),
                    color: Theme.of(context).scaffoldBackgroundColor,
                    boxShadow: const <BoxShadow>[
                      BoxShadow(
                          color: Color(0x27100f7c),
                          offset: Offset(0, 2),
                          blurRadius: 10,
                          spreadRadius: 4)
                    ],
                  )
                : const BoxDecoration(),
            child: GestureDetector(
              onTap: () async => _selectDate(context),
              child: AbsorbPointer(
                absorbing: true,
                child: TextField(
                  focusNode: focusNode,
                  style: TextStyles.formsTextFieldText,
                  textAlign: TextAlign.right,
                  controller: widget.controller,
                  onChanged: (String? value) {
                    if (widget.onChanged == null || value == null) {
                      return;
                    }
                    else {
                      widget.onChanged!(value);
                    }
                  },
                  decoration: InputDecoration(
                    suffixIcon: widget.icon.isEmpty
                        ? null
                        : Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: SvgPicture.asset(widget.icon),
                          ),
                    suffixIconConstraints:
                        const BoxConstraints(maxHeight: 35, maxWidth: 35),
                    contentPadding:
                        const EdgeInsets.only(bottom: 40 / 2, right: 7),
                    border: const OutlineInputBorder(
                      borderSide: BorderSide(color: AppColor.fillNavyNavy25),
                      borderRadius: BorderRadius.all(
                        Radius.circular(6.0),
                      ),
                    ),
                    enabledBorder: const OutlineInputBorder(
                      borderSide: BorderSide(color: AppColor.fillNavyNavy25),
                      borderRadius: BorderRadius.all(
                        Radius.circular(6.0),
                      ),
                    ),
                    focusedBorder: const OutlineInputBorder(
                      borderSide: BorderSide(color: AppColor.navy),
                      borderRadius: BorderRadius.all(
                        Radius.circular(6.0),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _selectDate(BuildContext context) async {
    DateTime selectedDate = DateTime.now();

    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1901, 1),
        lastDate: DateTime(2100));
    if (picked != null && picked != selectedDate) {
      String formattedDateString = Formatters.dateToFormattedString(picked);
      widget.controller.value = TextEditingValue(
        text: formattedDateString,
        selection: TextSelection.fromPosition(
          TextPosition(offset: formattedDateString.length),
        ),
      );
    }
  }
}
