

import 'package:digitalis_shared/src/ui/styles/app_color.dart';
import 'package:digitalis_shared/src/ui/styles/text_styles.dart';
import 'package:digitalis_shared/src/ui/utils/size_config.dart';
import 'package:digitalis_shared/src/ui/utils/validators.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';


class CustomTextField extends StatefulWidget {
  final String title;
  final String? hint;
  final bool editable;
  final TextEditingController controller;
  final double width;
  final ValueChanged<bool>? onChanged;
  final bool isNotes;
  final String icon;
  final bool isNumber;
  final int charLimit;
  final String? Function(String?) validator;


  const CustomTextField({
    Key? key,
    required this.title,
    this.hint,
    required this.controller,
    this.width = 165,
    this.onChanged,
    this.editable = true,
    this.isNotes = false,
    this.icon = '',
    this.charLimit = 0,
    this.isNumber = false,
    this.validator = Validators.emptyValidator}) : super(key: key);

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  late FocusNode focusNode;
  String _errorText = '';

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
    focusNode.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: widget.isNotes ? SizeConfig.screenHeight * 0.25 : 72,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.title,
              style: TextStyles.formsTextFieldLabel2,
              textAlign: TextAlign.right),
          const SizedBox(height: 5),
          Container(
            height: widget.isNotes ? SizeConfig.screenHeight * 0.2 : 35,
            width: widget.width,
            decoration: focusNode.hasFocus
                ? BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(6.0)),
                    color: Theme.of(context).scaffoldBackgroundColor,
                    boxShadow: const <BoxShadow>[
                      BoxShadow(
                          color: Color(0x27100f7c),
                          offset: Offset(0, 2),
                          blurRadius: 10,
                          spreadRadius: 4)
                    ],
                  )
                : const BoxDecoration(),
            child: TextFormField(
              scrollPadding: widget.isNotes ? const EdgeInsets.only(bottom:180) :const EdgeInsets.only(bottom:60),
              textInputAction: widget.isNotes ? null : TextInputAction.go,
              keyboardType: widget.isNumber ? TextInputType.number : null,
              enabled: widget.editable,
              maxLines: 10,
              validator: (value) {
                String? hasError = widget.validator(value);
                if (hasError != null) {
                  // Future delayed is used so that we won't run setState during build operation
                  Future.delayed(Duration.zero, () {
                    setState(() {
                      _errorText = hasError;
                    });
                  });
                  return _errorText;
                }
                Future.delayed(Duration.zero, () {
                  setState(() {
                    _errorText = '';
                  });

                });
                return null;
              },
              onChanged: (String? value) {
                if (widget.validator == Validators.phoneValidator &&
                    widget.editable) {
                  if (value == null || widget.onChanged == null) return;
                  widget.onChanged!(value.length == 7);
                }
              },
              focusNode: focusNode,
              style: TextStyles.formsTextFieldText,
              textAlign: TextAlign.right,
              controller: widget.controller,
              maxLength: widget.charLimit != 0 ? widget.charLimit : null,
              decoration: InputDecoration(
                counterText: '',
                suffixIcon: widget.icon.isEmpty
                    ? null
                    : Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: SvgPicture.asset(widget.icon),
                      ),
                suffixIconConstraints:
                    const BoxConstraints(maxHeight: 35, maxWidth: 35),
                // Parameter "height: 0.001" fixes the Flutter 3.0.5 and above issue
                // to more info see here
                // https://stackoverflow.com/questions/56426262/how-to-remove-error-message-in-textformfield-in-flutter
                errorStyle: const TextStyle(fontSize: 0, height: 0.001),
                counterStyle: const TextStyle(fontSize: 0, height: 0),
                contentPadding: const EdgeInsets.only(bottom: 40 / 2, right: 7),
                border: const OutlineInputBorder(
                  borderSide: BorderSide(color: AppColor.fillNavyNavy25),
                  borderRadius: BorderRadius.all(
                    Radius.circular(6.0),
                  ),
                ),
                enabledBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: AppColor.fillNavyNavy25),
                  borderRadius: BorderRadius.all(
                    Radius.circular(6.0),
                  ),
                ),
                focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: AppColor.navy),
                  borderRadius: BorderRadius.all(
                    Radius.circular(6.0),
                  ),
                ),
                disabledBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: AppColor.fillNavyNavy10),
                  borderRadius: BorderRadius.all(
                    Radius.circular(6.0),
                  ),
                ),
              ),
            ),
          ),
          _errorText.isNotEmpty || widget.hint == null
              ? const SizedBox()
              : Text(widget.hint ?? "",
                  style: TextStyles.formsTextFieldComment,
                  textAlign: TextAlign.right),
          _errorText.isNotEmpty
              ? Text(_errorText,
                  style: TextStyles.formsTextFieldError.copyWith(height: 1.2),
                  textAlign: TextAlign.center)
              : const SizedBox()
        ],
      ),
    );
  }
}
