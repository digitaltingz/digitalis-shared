import 'package:flutter/material.dart';

import '../../../../digitalis_shared.dart';

class CustomIndicator extends StatelessWidget {
  final IndicatorState indicatorState;
  final color;

  const CustomIndicator({required this.indicatorState, this.color = AppColor.navy});

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: indicatorState == IndicatorState.ACTIVE
          ? 1
          : indicatorState == IndicatorState.VISIT
          ? 0.5
          : 0.1,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 8.0),
        height: 10.0,
        width: 10.0,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: this.color,
        ),
      ),
    );
  }
}
