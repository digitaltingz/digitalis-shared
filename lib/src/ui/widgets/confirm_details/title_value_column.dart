import 'package:digitalis_shared/src/ui/styles/text_styles.dart';
import 'package:digitalis_shared/src/ui/utils/size_config.dart';
import 'package:flutter/material.dart';

class TitleValueColumn extends StatelessWidget {
  final String title;
  final String content;
  final bool fullWidth;
  final int numOfColumns;

  const TitleValueColumn({
    Key? key,
    required this.title,
    required this.content,
    this.fullWidth = false,
    this.numOfColumns = 3,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: fullWidth
          ? SizeConfig.screenWidth - 60
          : (SizeConfig.screenWidth - 60) / numOfColumns,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title,
              style: TextStyles.formsDataLabel, textAlign: TextAlign.right),
          const SizedBox(height: 5),
          Text(
            content,
            style: TextStyles.titlesH516NavyRight,
            textAlign: TextAlign.right,
            overflow: TextOverflow.ellipsis,
          ),
          const SizedBox(height: 10),
        ],
      ),
    );
  }
}
