import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../digitalis_shared.dart';

class ConfirmHeader extends StatelessWidget {
  final String imagePath;
  final String title;
  final bool hasEditIcon;
  final Function? navigateTo;

  const ConfirmHeader({Key? key, required this.imagePath, required this.title, this.hasEditIcon = false , this.navigateTo})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                SvgPicture.asset(imagePath),
                const SizedBox(width: 5),
                Text(
                  title,
                  style: TextStyles.titlesH318NavyRight,
                  textAlign: TextAlign.right,
                ),
              ],
            ),
            hasEditIcon ? InkWell(onTap: ()=> {
              navigateTo!()
            }
                ,child: SvgPicture.asset('asset/icons/edit_icon.svg')) : const SizedBox()
          ],
        ),
        const SizedBox(height: 10),
        Divider(
          height: 1,
          thickness: 1,
        ),
      ],
    );
  }
}
