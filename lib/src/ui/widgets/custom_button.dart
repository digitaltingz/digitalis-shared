import 'package:digitalis_shared/src/ui/styles/app_color.dart';
import 'package:digitalis_shared/src/ui/styles/text_styles.dart';
import 'package:digitalis_shared/src/ui/utils/size_config.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final bool largerWidth;
  final bool isFilled;
  final bool isEnabled;
  final Function() onTap;

  const CustomButton({
    Key? key,
    required this.text,
    required this.onTap,
    this.largerWidth = false,
    this.isEnabled = true,
    this.isFilled = true}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AbsorbPointer(
      absorbing: !isEnabled,
      child: InkWell(
        onTap: onTap,
        child: Container(
          width: largerWidth
              ? SizeConfig.screenWidth * 0.8
              : SizeConfig.screenWidth * 0.4,
          height: 44,
          decoration: BoxDecoration(
              border: isFilled
                  ? const Border()
                  : Border.all(color: AppColor.navy, width: 2),
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              boxShadow: isFilled && isEnabled
                  ? [
                      const BoxShadow(
                          color: Color(0x40100f7c),
                          offset: Offset(0, 10),
                          blurRadius: 20,
                          spreadRadius: 0)
                    ]
                  : [],
              color: isFilled ? AppColor.navy : Colors.transparent),
          child: Center(
            child: Text(
              text,
              style: isFilled
                  ? isEnabled
                      ? TextStyles.titlesH416WhiteCenter
                      : TextStyles.titlesH416WhiteCenterDisabled
                  : TextStyles.cTANavy,
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
