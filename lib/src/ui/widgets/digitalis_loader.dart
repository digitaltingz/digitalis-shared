

import 'package:digitalis_shared/src/ui/styles/app_color.dart';
import 'package:flutter/material.dart';

class DigitalisLoader extends StatelessWidget {
  final bool isBlue;

  const DigitalisLoader({Key? key, this.isBlue = true}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 45,
        width: 45,
        child: CircularProgressIndicator(
          color: isBlue ?AppColor.navy : AppColor.white,
          strokeWidth: 3,
        ),
      ),
    );
  }
}

