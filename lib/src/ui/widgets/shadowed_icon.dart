

import 'package:digitalis_shared/src/ui/styles/app_color.dart';
import 'package:flutter/material.dart';


class ShadowedIcon extends StatelessWidget {

  final String iconPath;

  const ShadowedIcon({
    Key? key,
    required this.iconPath}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: const <BoxShadow>[
          BoxShadow(
              color: AppColor.cayan,
              offset: Offset(-3, 3),
              spreadRadius: 0,
              blurRadius: 0),
        ],
        border: Border.all(color: AppColor.navy, width: 1),
        borderRadius: const BorderRadius.all(
          Radius.circular(4),
        ),
      ),
      child: Image.asset(iconPath),
    );
  }
}
