

import 'package:digitalis_shared/src/ui/styles/app_color.dart';
import 'package:digitalis_shared/src/ui/styles/text_styles.dart';
import 'package:flutter/material.dart';


class CustomCheckBox extends StatefulWidget {
  final bool isChecked;
  final String text;
  final void Function(bool?) onChange;

  const CustomCheckBox({
    Key? key,
    required this.isChecked,
    required this.onChange,
    required this.text}) : super(key: key);

  @override
  State<CustomCheckBox> createState() => _CustomCheckBoxState();
}


class _CustomCheckBoxState extends State<CustomCheckBox> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ClipRRect(
          clipBehavior: Clip.hardEdge,
          borderRadius: const BorderRadius.all(Radius.circular(6)),
          child: SizedBox(
            width: 27,
            height: 27,
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: AppColor.navy,
                  width: 2,
                ),
                borderRadius: BorderRadius.circular(5),
              ),
              child: Theme(
                data: ThemeData(
                  unselectedWidgetColor: Colors.transparent,
                ),
                child: Checkbox(
                  fillColor: MaterialStateProperty.all<Color>(Colors.transparent),
                  checkColor: AppColor.navy,
                  value: widget.isChecked,
                  onChanged: widget.onChange,
                ),
              ),
            ),
          ),
        ),
        const SizedBox(width: 12),
        Expanded(
          child: Text(
            widget.text,
            style: TextStyles.checkBoxTextStyleCenter,
            textAlign: TextAlign.right,
          ),
        ),
      ],
    );
  }
}
